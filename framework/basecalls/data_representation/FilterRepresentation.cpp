#include "FilterRepresentation.h"

using namespace std;

FilterRepresentation::FilterRepresentation(std::vector<uint16_t> &lanes, std::vector<uint16_t> &tiles) {
    for (auto lane : lanes) {
        this->filterData[lane] = map<uint16_t, vector<char>>();
        for (auto tile : tiles) this->filterData[lane][tile] = vector<char>();
    }
}

void FilterRepresentation::addFilterData(uint16_t lane, uint16_t tile, vector<char> tileFilterData) {
    this->filterData[lane][tile] = tileFilterData;
}

void FilterRepresentation::initializeDefault(uint16_t lane, uint16_t tile, int numRead){
    this->filterData[lane][tile] = vector<char>(numRead, 1);
}

char FilterRepresentation::getFilterDataForPosition(uint16_t lane, uint16_t tile, unsigned long position) {
    return this->filterData[lane][tile][position];
}

vector<char>& FilterRepresentation::getFilterData(uint16_t lane, uint16_t tile) {
    return this->filterData[lane][tile];
}

uint32_t FilterRepresentation::getNumSequences(uint16_t lane, uint16_t tile) {
    return this->filterData[lane][tile].size();
}

