#ifndef LIVEKIT_FILTERREPRESENTATION_H
#define LIVEKIT_FILTERREPRESENTATION_H

#include <map>
#include <vector>

class FilterRepresentation {
private:
    /**
     * This map contains all information about which sequence should be filtered
     * usage: filterData[lane][tile][position_in_bcl]
     */
    std::map<uint16_t, std::map<uint16_t, std::vector<char>>> filterData;

public:
    /**
     * Creates a FilterRepresentation and initializes the filterData which stores information about which sequence gets filtered
     * @param lanes
     * @param tiles
     */
    FilterRepresentation(std::vector<uint16_t> &lanes, std::vector<uint16_t> &tiles);

    /**
     * Inserts tileFilterData into the map 'filterData' at the specified tile
     * @param lane
     * @param tile
     * @param filterData
     */
    void addFilterData(uint16_t lane, uint16_t tile, std::vector<char> tileFilterData);

    /**
     * This initializes the vector with a lot of "1" and sets the numRead
     * This is used in case no filter file exists (therefore the whole bcl file should be used)
     */
    void initializeDefault(uint16_t lane, uint16_t tile, int numRead);

    /**
     * @param lane
     * @param tile
     * @param position is 0-based; position is a number between 0 and getNumSequences(lane, tile) - 1
     * @return the information about whether the base at the specified position should be filtered (0 means "do filter")
     */
    virtual char getFilterDataForPosition(uint16_t lane, uint16_t tile, unsigned long position);

    /**
     * @param lane
     * @param tile
     * @return returns filter data for all sequences of a specified tile
     */
    virtual std::vector<char>& getFilterData(uint16_t lane, uint16_t tile);

    /**
     * Return the size of the parsed filter data of the specified tile.
     * This size specifies how many sequences exist for the specified tile
     * @param lane
     * @param tile
     * @return
     */
    virtual uint32_t getNumSequences(uint16_t lane, uint16_t tile);

    virtual ~FilterRepresentation() = default;
};
#endif //LIVEKIT_FILTERREPRESENTATION_H
