#ifndef LIVEKIT_BCLPARSER_H
#define LIVEKIT_BCLPARSER_H

#include <sstream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include "../data_representation/BclRepresentation.h"

class BclParser {
protected:
    BclRepresentation *bclRepresentation;

public:
    BclParser(BclRepresentation *bclRepresentation) :
            bclRepresentation(bclRepresentation) {}

    virtual void parse(uint16_t lane, uint16_t tile, uint16_t cycle, const std::string &bclPath);

    virtual void parse(uint16_t lane, uint16_t tile, std::vector<char> &rawBclData);

    virtual ~BclParser() = default;
};

#endif //LIVEKIT_BCLPARSER_H
