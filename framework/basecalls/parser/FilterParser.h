#ifndef LIVEKIT_FILTERPARSER_H
#define LIVEKIT_FILTERPARSER_H

#include <sstream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include <map>
#include "../data_representation/FilterRepresentation.h"

class FilterParser {
protected:
    std::vector<uint16_t> &lanes;

    std::vector<uint16_t> &tiles;

    FilterRepresentation *filterRepresentation;

public:
    FilterParser(std::vector<uint16_t> &lanes, std::vector<uint16_t> &tiles, FilterRepresentation* filterRepresentation);

    void parse(uint16_t lane, uint16_t tile, const std::string &filterPath, int numReads);

    void parse(uint16_t lane, uint16_t tile, std::vector<char> &rawFilterData);

    ~FilterParser() = default;
};

#endif //LIVEKIT_FILTERPARSER_H
