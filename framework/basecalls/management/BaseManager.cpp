#include <utility>

#include "../parser/AggregatedBclParser.h"
#include "../parser/CompressedBclParser.h"
#include "../parser/BgzfBclParser.h"

#include "BaseManager.h"

using namespace std;

#define revtwobit_repr(n) ((n) == 0 ? 'A' : \
                           (n) == 1 ? 'C' : \
                           (n) == 2 ? 'G' : 'T')

BaseManager::BaseManager(string baseCallRootDirectory, vector<uint16_t> lanes, vector<uint16_t> tiles, const string& bclParserType, vector<pair<int, char>> readStructure) :
          baseCallRootDirectory(move(baseCallRootDirectory)),
          lanes(std::move(lanes)),
          tiles(std::move(tiles)),
          sequenceManager(this->lanes, this->tiles),
          bclData(this->lanes, this->tiles),
          filterData(this->lanes, this->tiles),
          filterParser(this->lanes, this->tiles, &filterData),
          readStructure(std::move(readStructure)) {
    if (bclParserType == "BclParser") {
        this->bclParser = new BclParser(&bclData);
    } else if (bclParserType == "CompressedBclParser"){
        this->bclParser = new CompressedBclParser(&bclData);
    } else if (bclParserType == "AggregatedBclParser") {
        this->bclParser = new AggregatedBclParser(&bclData, &filterData);
    } else if (bclParserType == "BgzfBclParser") {
        this->bclParser = new BgzfBclParser(&bclData);
    } else {
        throw std::runtime_error("Wrong BclParser configured");
    }
}

void BaseManager::processCycle(int cycleNumber, bool firstCycle) {
    // for each lane, tile extend sequences from bcl
    for (auto &lane : lanes)
        for (auto &tile : tiles) {
            bclParser->parse(lane, tile, cycleNumber, this->baseCallRootDirectory);

            if (firstCycle) {
                uint32_t bclSize = bclData.getBclSize(lane, tile);
                sequenceManager.getSequences(lane, tile).setSize(bclSize, this->readStructure);
                filterParser.parse(lane, tile, this->baseCallRootDirectory, bclSize);
                if (this->filterData.getNumSequences(lane, tile) != bclSize)
                    cerr << "The filter file in lane "
                         << to_string(lane) << ", tile " << to_string(tile)
                         << " has a different number of reads as the corresponding bcl file in the first cycle."
                         << endl;
            }

            const BCL& lastBaseCall = bclData.getBcl(lane, tile, 0);
            // TODO: The sequenceManager takes a significant part of the BaseManger-runtime - can this be improved?
            sequenceManager.getSequences(lane, tile).extendSequences(lastBaseCall);
        }

    //sequenceManager.writeAllSequencesToDisk(cycleNumber);
}

SequenceContainer &BaseManager::getSequences(uint16_t lane, uint16_t tile) {
    return this->sequenceManager.getSequences(lane, tile);
}

Sequence &BaseManager::getSequence(uint16_t lane, uint16_t tile, int sequence) {
    return this->sequenceManager.getSequences(lane, tile).getSequence(sequence);
}

const BCL &BaseManager::getBcl(uint16_t lane, uint16_t tile, uint16_t cycleOffset) {
    return bclData.getBcl(lane, tile, cycleOffset);
}

const deque<BCL> &BaseManager::getBcls(uint16_t lane, uint16_t tile) {
    return bclData.getBcls(lane, tile);
}

deque<BCL> BaseManager::getMostRecentBcls(uint16_t lane) {
    return bclData.getMostRecentBcls(lane);
}

bool BaseManager::filterBasecall(uint16_t lane, uint16_t tile, unsigned long position) {
    return 0 == this->filterData.getFilterDataForPosition(lane, tile, position);
}

vector<char>& BaseManager::getFilterData(uint16_t lane, uint16_t tile) {
    return this->filterData.getFilterData(lane, tile);
}

uint32_t BaseManager::getNumSequences(uint16_t lane, uint16_t tile) {
    return this->filterData.getNumSequences(lane, tile);
}

vector<char> BaseManager::getSampleBarcode(Read barcode, vector<vector<string>> barcodeVector, vector<uint16_t> barcodeErrors, bool isSingleEnd) {

    int faultCounter = 0;
    bool isSampleBarcode = true;

    // Compare each barcode of the barcodeVector with the barcode of the current read
    for (int i = 0; i < (int) barcodeVector.size(); i++) {
        string concatedBarcode;
        if (isSingleEnd) {
            concatedBarcode = barcodeVector[i][0];
        }
        else {
            concatedBarcode = barcodeVector[i][0] + barcodeVector[i][1];
        }
        // Compare each base
        for (int j = 0; j < (int) barcode.size(); j++) {
            char base = barcode[j];
            if (base != concatedBarcode.at(j)) {
                faultCounter++;
            }

            int fragmentNumber = 0;
            if (j > (int)barcodeVector[i][0].size()) { fragmentNumber += 1;}

            if (faultCounter > barcodeErrors[fragmentNumber]) {
                isSampleBarcode = false;
                // Try the next barcode of the barcodeVector
                break;
            }
        }
        faultCounter = 0;
        if (isSampleBarcode) {
            vector<char> charBarcode(concatedBarcode.begin(), concatedBarcode.end());
            return charBarcode;
        }
        isSampleBarcode = true;
    }

    return {};
}

BaseManager::~BaseManager() {
    delete this->bclParser;
}
