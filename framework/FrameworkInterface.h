#ifndef LIVEKIT_FRAMEWORKINTERFACE_H
#define LIVEKIT_FRAMEWORKINTERFACE_H

#include <memory>
#include "basecalls/data_representation/SequenceContainer.h"
#include "basecalls/data_representation/BclRepresentation.h"
#include "basecalls/management/BaseManager.h"
#include "CycleManager.h"

class Framework;
class FragmentContainer;
class Fragment;

/**
 * Interface of the whole framework.
 * Plugins have access to theses functions.
 */
class FrameworkInterface {
private:
    Framework *framework;
public:
    explicit FrameworkInterface(Framework* framework);

    FrameworkInterface(const FrameworkInterface &frameworkInterface);

    /**
     * Get all sequences from a specific lane and tile.
     * @param lane
     * @param tile
     * @return
     */
    virtual SequenceContainer &getSequences(uint16_t lane, uint16_t tile);

    /**
     * Get a specific Sequence from a specific lane and tile.
     * @param lane
     * @param tile
     * @param sequence Index of the wanted sequence
     * @return
     */
    virtual const Sequence &getSequence(uint16_t lane, uint16_t tile, int sequence);

    /**
     * Get basecalls of all reads of a specific cycle where cycleOffset = 0 is the latest cycle.
     * @param lane
     * @param tile
     * @param cycleOffset
     * @return
     */
    virtual const BCL &getBcl(uint16_t lane, uint16_t tile, int cycleOffset);

    /**
     * Get basecalls of the last 10 cycles.
     * @param lane
     * @param tile
     * @return
     */
    virtual const std::deque<BCL> &getBcls(uint16_t lane, uint16_t tile);

    /**
     * Get the latest basecalls of all tiles of one specific lane.
     * @param lane
     * @return
     */
    virtual std::deque<BCL> getMostRecentBcls(uint16_t lane);

    /**
     * Uses .filter files to determine if a read should be filtered or not.
     * @param lane
     * @param tile
     * @param position of the read in the sequence representation
     * @return true if the read is filtered and false otherwise
     */
    virtual bool filterBasecall(uint16_t lane, uint16_t tile, uint16_t position);

    /**
     * Get the number of reads (sequences) of one specific tile and lane.
     *
     * @param lane
     * @param tile
     * @return
     */
    virtual uint32_t getNumSequences(uint16_t lane, uint16_t tile);

    /**
     * Return the current cycle (starting by 1). In FullReadPostProcess the currentCycle is equal to getCycleCount + 1.
     * @return
     */
    virtual int getCurrentCycle();

    /**
     * Get the total number of cycles.
     * Is calculated by the read structure (e.g. 100R 8B 8B 100R = 216 cycles).
     * @return
     */
    virtual int getCycleCount();

    /**
     * Get the current cycle of the current read. (e.g. when the read structure ist 100R 8B 8B 100R and the current
     * cycle is 123, the current read cycle is 7, because it is the 7-th cycle of the fourth read.
     * @return
     */
    virtual int getCurrentReadCycle();

    /**
     * Get the index of the current read.
     * @return
     */
    virtual int getCurrentReadId();

    /**
     * Get the length of the current read.
     * The read length does not depend on the length of the other reads in that sequence.
     * @return
     */
    virtual int getCurrentReadLength();

    /**
     * LiveKit distinguish between read and barcode.
     * @return true if a barcode was sequenced this cycle or false otherwise
     */
    virtual bool isBarcodeCycle();

    /**
     * A mate is a single read, that is not a barcoe. In a paired end read structure, the two reads are two mates.
     * All mates are numbered and 1-based.
     * @return mate id of the current read. If the current cycle is an barcode cycle, then this method returns 0
     */
    virtual int getCurrentMateId();

    /**
     * Get the total number of mates.
     * @return
     */
    virtual int getMateCount();

    /**
     * Create a new fragment, that can be received by other plugins.
     * @param name of the new fragment
     * @return
     */
    virtual std::shared_ptr<Fragment> createNewFragment(std::string name);

    /**
     * Create a new Fragment Container.
     * Contains all Fragments of one Plugin.
     * @return
     */
    virtual FragmentContainer *createNewFragmentContainer();

    /**
     * The base manager handles the basecalls, barcodes and reads.
     * @return
     */
    virtual BaseManager* getBaseManager();

    /**
     * Check if a read is part of the barcodeVector given a maximum number of errors and if the read is is single-end.
     * @param barcode the read to check
     * @param barcodeVector the vector with all barcodes to check
     * @param barcodeErrors the number of allowed errors per barcode
     * @param isSingleEnd bool which specifies if the read is single-end
     * @return the correct barcode from the vector if the barcode matches otherwise an empty vector is returned
     */
    virtual std::vector<char> getSampleBarcode(Read barcode, std::vector<std::vector<std::string>> barcodeVector, std::vector<uint16_t> barcodeErrors, bool isSingleEnd);
};


#endif //LIVEKIT_FRAMEWORKINTERFACE_H
