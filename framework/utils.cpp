#include "utils.h"

using namespace std;

ifstream::pos_type getFilesize(const string &fileName) {
    ifstream in(fileName, ios::binary | ios::ate);
    return in.tellg();
}

void readBinaryFile(const string &fileName, vector<char> **rawData) {
    // get file size
    uint64_t size = (uint64_t) getFilesize(fileName);

    // open binary file
    FILE *file;
    file = fopen(fileName.c_str(), "rb");

    if (!file) {
        stringstream error;
        error << "Error reading binary file " << fileName << ": Could not open file.";
        throw ios::failure(error.str());
    }

    // allocate memory
    *rawData = new vector<char>(size);
    vector<char> &data = **rawData;

    // read all data at once
    uint64_t read = fread(data.data(), 1, size, file);

    if (read != size) {
        stringstream error;
        error << "Error reading binary file " << fileName << ": Read " << read << " bytes while file has " << size
              << " bytes.";
        throw ios::failure(error.str());
    }

    fclose(file);
}

string toNDigits(int value, int N, char fill_char) {
    stringstream ss;
    ss << setw(N) << setfill(fill_char) << value;
    return ss.str();
}

string getBclFilename(uint16_t lane, uint16_t tile, uint16_t cycle, const string &path, bool compressed) {
    ostringstream path_stream;
    path_stream << path << "/L" << toNDigits(lane, 3) << "/C" << cycle << ".1/s_" << lane << "_"
                << tile << ".bcl";
    if (compressed)
        path_stream << ".gz";
    return path_stream.str();
}

string getBgzfFilename(uint16_t lane, uint16_t cycle, const string &path, bool bci) {
    ostringstream path_stream;
    path_stream << path << "/L" << toNDigits(lane, 3) << "/" << toNDigits(cycle, 4) << ".bcl.bgzf";
    if (bci)
        path_stream << ".bci";
    return path_stream.str();
}

string getBciFilename(uint16_t lane, const string &path) {
    ostringstream path_stream;
    path_stream << path << "/L" << toNDigits(lane, 3) << "/s_" << lane << ".bci";
    return path_stream.str();
}

string getFilterFilename(uint16_t lane, uint16_t tile, const string &path) {
    ostringstream path_stream;
    path_stream << path << "/L" << toNDigits(lane, 3) << "/s_" << lane << "_"
                << tile << ".filter";
    return path_stream.str();
}

string getCBclFilename(uint16_t lane, uint16_t surface, uint16_t cycle, const string &path) {
    ostringstream pathStream;
    pathStream << path << "/L" << toNDigits(lane, 3) << "/C" << cycle << ".1/L" << toNDigits(lane, 3)
               << "_" << surface << ".cbcl";
    return pathStream.str();
}

bool sameFile(struct stat stat1, struct stat stat2) {
    return (stat1.st_dev == stat2.st_dev) && (stat1.st_ino == stat2.st_ino);
}

void serializeString(const std::string &string, FILE *serializeFile) {
    unsigned long size = string.size();
    fwrite(&size, sizeof(unsigned long), 1, serializeFile);
    fwrite(string.c_str(), size, 1, serializeFile);
}

void serializeString(const std::string &string, char **serializedSpace) {
    unsigned long size = string.size();
    memcpy(*serializedSpace, &size, sizeof(unsigned long));
    *serializedSpace += sizeof(unsigned long);
    memcpy(*serializedSpace, string.c_str(), size);
    *serializedSpace += size;
}
