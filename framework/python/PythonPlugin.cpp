#include "PythonPlugin.h"
#include "PythonFrameworkInterface.h"

typedef std::vector<std::string> StringList;

using namespace std;

bp::object PythonPlugin::import(const std::string &path) {
    string module = this->getFileNameFromPath(path);
    bp::object globals = bp::import("__main__").attr("__dict__");

    bp::dict locals;
    locals["module_name"] = module;
    locals["path"] = path;

    bp::exec("import imp\n"
             "new_module = imp.load_module(module_name, open(path), path, ('py', 'U', imp.PY_SOURCE))\n",
             globals,
             locals);
    return locals["new_module"];
}

string PythonPlugin::getFileNameFromPath(string path) {
    // Remove directory
    const size_t last_slash_idx = path.find_last_of('/');
    if (std::string::npos != last_slash_idx) {
        path.erase(0, last_slash_idx + 1);
    }

    // Remove extension
    const size_t period_idx = path.find_last_of('.');
    if (std::string::npos != period_idx) {
        path.erase(period_idx);
    }

    return path;
}

FragmentContainer *PythonPlugin::runPythonMethod(const char *name, FragmentContainer *inputFragments = nullptr) {
    if (inputFragments != nullptr) {
        vector<string> fragmentNames;
        //serialize all fragments so python plugins can access them
        for (const auto &fragment : inputFragments->getAllFragments()) {
            fragment->serialize();
            fragmentNames.push_back(fragment->name + "_" + fragment->getHash());
        }
        try {
            this->pluginHandle.attr(name)((StringList) fragmentNames);
        }
        catch (const bp::error_already_set &) {
            std::cerr << "Python Error: ";
            PyErr_Print();
            throw runtime_error("<<< Python exception caught!");
        }
    } else {
        try {

            this->pluginHandle.attr(name)();
        }
        catch (const bp::error_already_set &) {
            std::cerr << "Python Error: ";
            PyErr_Print();
            throw runtime_error("<<< Python exception caught!");
        }
    }

    return nullptr;
}

void PythonPlugin::setConfig() {
    this->registerConfigEntry<string>("name", "NAME_MISSING");
}

void PythonPlugin::init() {

    try {
        auto module = this->import(this->specification->pluginPath);

        this->pluginHandle = module.attr(this->getConfigEntry<string>("name").c_str())();

        this->pluginHandle.attr("setFramework")(PythonFrameworkInterface(this->framework));

        this->pluginHandle.attr("loadConfig")(this->specification->pluginConfigPath);

        this->runPythonMethod("init");
    }
    catch (const bp::error_already_set &) {
        std::cerr << "Python Error: ";
        PyErr_Print();
        throw runtime_error("<<< Python exception caught!");
    }
}

FragmentContainer *PythonPlugin::runPreprocessing(FragmentContainer *inputFragments) {
    return this->runPythonMethod("runPreprocessingHook", inputFragments);
}

FragmentContainer *PythonPlugin::runCycle(FragmentContainer *inputFragments) {
    return this->runPythonMethod("runCycleHook", inputFragments);
}

FragmentContainer *PythonPlugin::runFullReadPostprocessing(FragmentContainer *inputFragments) {
    return this->runPythonMethod("runFullReadPostprocessingHook", inputFragments);
}

void PythonPlugin::finalize() {
    this->runPythonMethod("finalize");
}