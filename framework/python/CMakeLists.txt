set(PYTHON_SOURCES
        ${CMAKE_CURRENT_SOURCE_DIR}/PythonDependencies.h
        ${CMAKE_CURRENT_SOURCE_DIR}/PythonDependencies.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/PythonFrameworkInterface.h
        ${CMAKE_CURRENT_SOURCE_DIR}/PythonPlugin.h
        ${CMAKE_CURRENT_SOURCE_DIR}/PythonPlugin.cpp
        PARENT_SCOPE)
