#ifndef LIVEKIT_UTILS_H
#define LIVEKIT_UTILS_H

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <sys/stat.h>

std::ifstream::pos_type getFilesize(const std::string &fileName);

void readBinaryFile(const std::string &fileName, std::vector<char> **rawData);

std::string toNDigits(int value, int N, char fill_char = '0');

std::string
getBclFilename(uint16_t lane, uint16_t tile, uint16_t cycle, const std::string &path, bool compressed = false);

std::string getFilterFilename(uint16_t lane, uint16_t tile, const std::string &path);

std::string getCBclFilename(uint16_t lane, uint16_t surface, uint16_t cycle, const std::string &path);

std::string getBgzfFilename(uint16_t lane, uint16_t cycle, const std::string &path, bool bci = false);

std::string getBciFilename(uint16_t lane, const std::string &path);

bool sameFile(struct stat stat1, struct stat stat2);

void serializeString(const std::string &string, FILE *serializeFile);

void serializeString(const std::string &string, char **serializedSpace);

#endif //LIVEKIT_UTILS_H
