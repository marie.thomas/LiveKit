#ifndef LIVEKIT_FRAMEWORK_H
#define LIVEKIT_FRAMEWORK_H

#include <dlfcn.h>
#include <vector>
#include <string>
#include <memory>

// This workaround is needed to make Boost.Python & Python-Plugins work on MacOS.
// "The problem is that pyport tries to replace the ctypes definion of the isascii(ch) and related functions by a
// replacement that works better with UTF-8 on FreeBSD and OSX. That replacement is incompatible with the localfwd.h
// header. (Source: https://trac.macports.org/ticket/44288)
// To resolve the problem, we undid the preprocessor definitions.
#ifdef __APPLE__
#include "pyport.h"
#undef toupper
#undef tolower
#undef isspace
#undef isupper
#undef islower
#undef isalpha
#undef isalnum
#endif

#pragma GCC diagnostic ignored "-Wenum-compare"

#include "CycleManager.h"
#include "basecalls/management/BaseManager.h"
#include "fragments/Fragment.h"
#include "fragments/FragmentContainer.h"
#include "configuration/Configurable.h"
#include "python/PythonDependencies.h"
#include "FrameworkInterface.h"
#include "execution_planning/PluginExecutionGraph.h"
#include "FileDetector.h"
#include "python/PythonPlugin.h"
#include "../serializables/SerializableFactory.h"

template<typename T>
struct LibraryInfo {
    void (*destroy)(T *);

    T *libraryInstance;
};

typedef LibraryInfo<Plugin> PluginInfo;

class Framework : public Configurable {
private:
    std::vector<PluginInfo> pluginInfos;
    std::vector<PythonPlugin *> pythonPlugins;

    BaseManager *baseManager;

    CycleManager *cycleManager;

    PluginExecutionGraph *graph;

    FrameworkInterface frameworkInterface;

    LibraryInfo<SerializableFactory> serializableFactory;

    void setConfig() override;

    Plugin *setupPlugin(PluginSpecification &spec);

    Plugin *loadBinaryPlugin(const std::string &pluginPath);

    Plugin *loadPythonPlugin();

    void loadSerializablesFactory(const std::string &pluginPath);

    bool hasEnding(std::string const &fullString, std::string const &ending);

    friend class FrameworkInterface;

public:

    /**
     * Creates an Framework and automatically initializes the data specified in the config-file
     * @param manifestFilePath The path to the config-file
     */
    explicit Framework(const std::string &manifestFilePath);

    /**
     * Loads the plugin at the specified path
     * @param pluginPath The path to the plugin
     * @return Returns the loaded plugin
     */
    Plugin *loadPlugin(const std::string &pluginPath);

    /**
     * Checks for necessary directories
     * Calls `runPreprocessing` before the first cycle, but after the first bcl data is written by illumina
     * Calls `processCycle` (from BaseManager) and `runCycle` for every cycle, for every plugin
     */
    void runAllCycles();

    /**
     * Calls `runFullReadPostprocessing` on every plugin
     */
    void runFullReadPostprocessing();

    /**
     * Destroys every Plugin
     */
    void unloadPlugins();

    /**
     * Loads the specified config-entries into the configMap
     * Initializes all plugins that are specified in the config-file
     * @param filename The path to the config-file
     */
    void setupFromManifestFile();

    void serializeAllFragments();

    void serializeFragment(std::string name);

    void deserializeAllFragments();

    /**
     * Load content from RunInfo.xml into the framework config
     * @param configFilePath The path to the config-file
     */
    void setConfigFromFile(const std::string &configFilePath) override;

    /**
     * Creates a new Fragment
     * By declaring this method as virtual, plugins can access it without binding the source code of the framework to
     * themselves. They can access the implementation in the framework via the virtual table, which is being created.
     * @param name This is the name by which the Fragment will be referenced in the FragmentContainer
     * @return The new created Fragment
     */
    virtual std::shared_ptr<Fragment> createNewFragment(std::string name);

    virtual FragmentContainer *createNewFragmentContainer();

    virtual FrameworkInterface *getFrameworkInterface();

    virtual ~Framework();
};

#endif //LIVEKIT_FRAMEWORK_H
