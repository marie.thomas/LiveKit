#include "FrameworkInterface.h"
#include "Framework.h"

using namespace std;

FrameworkInterface::FrameworkInterface(Framework *framework) : framework(framework) {}

FrameworkInterface::FrameworkInterface(
        const FrameworkInterface &frameworkInterface) { this->framework = frameworkInterface.framework; }

SequenceContainer &FrameworkInterface::getSequences(uint16_t lane, uint16_t tile) {
    return this->framework->baseManager->getSequences(lane, tile);
}

const Sequence &FrameworkInterface::getSequence(uint16_t lane, uint16_t tile, int sequence) {
    return this->framework->baseManager->getSequence(lane, tile, sequence);
}

const BCL &FrameworkInterface::getBcl(uint16_t lane, uint16_t tile, int cycleOffset) {
    return this->framework->baseManager->getBcl(lane, tile, cycleOffset);
}

const deque<BCL> &FrameworkInterface::getBcls(uint16_t lane, uint16_t tile) {
    return this->framework->baseManager->getBcls(lane, tile);
}

deque<BCL> FrameworkInterface::getMostRecentBcls(uint16_t lane) {
    return this->framework->baseManager->getMostRecentBcls(lane);
}

bool FrameworkInterface::filterBasecall(uint16_t lane, uint16_t tile, uint16_t position) {
    return this->framework->baseManager->filterBasecall(lane, tile, position);
}

uint32_t FrameworkInterface::getNumSequences(uint16_t lane, uint16_t tile) {
    return this->framework->baseManager->getNumSequences(lane, tile);
}

int FrameworkInterface::getCurrentCycle() {
    return this->framework->cycleManager->getCurrentCycle();
}

int FrameworkInterface::getCycleCount() {
    return this->framework->cycleManager->getCycleCount();
}

int FrameworkInterface::getCurrentReadCycle() {
    return this->framework->cycleManager->getCurrentReadCycle();
}

int FrameworkInterface::getCurrentReadId() {
    return this->framework->cycleManager->getCurrentReadId();
}

int FrameworkInterface::getCurrentReadLength() {
    return this->framework->cycleManager->getCurrentReadLength();
}

bool FrameworkInterface::isBarcodeCycle() {
    return this->framework->cycleManager->isBarcodeCycle();
}

int FrameworkInterface::getCurrentMateId() {
    return this->framework->cycleManager->getCurrentMateId();
}

int FrameworkInterface::getMateCount() {
    return this->framework->cycleManager->getMateCount();
}

shared_ptr<Fragment> FrameworkInterface::createNewFragment(string name) {
    return this->framework->createNewFragment(name);
}

FragmentContainer *FrameworkInterface::createNewFragmentContainer() {
    return this->framework->createNewFragmentContainer();
}

vector<char> FrameworkInterface::getSampleBarcode (Read barcode, vector<vector<string>> barcodeVector, vector<uint16_t> barcodeErrors, bool isSingleEnd) {
    return this->framework->baseManager->getSampleBarcode(barcode, barcodeVector, barcodeErrors, isSingleEnd);
}


BaseManager *FrameworkInterface::getBaseManager() {
    return this->framework->baseManager;
}
