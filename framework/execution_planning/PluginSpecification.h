#ifndef LIVEKIT_PLUGINSPECIFICATION_H
#define LIVEKIT_PLUGINSPECIFICATION_H

#include "../fragments/Fragment.h"
#include <boost/property_tree/ptree.hpp>
#include <vector>

class PluginSpecification {

public:
    struct FragmentSpecification {
        std::string fragmentName;
        std::string fragmentType;
        bool fromPrecedingCycle = false;
    };

    PluginSpecification(const std::string &pluginPath, const std::string &pluginConfigPath);

    PluginSpecification(const std::string &pluginPath,
                        const std::string &pluginConfigPath,
                        const std::map<std::string, std::vector<FragmentSpecification>> &inputFragmentSpecifications,
                        const std::map<std::string, std::vector<FragmentSpecification>> &outputFragmentSpecifications
    );

    explicit PluginSpecification(const boost::property_tree::ptree &pluginPtree);

    std::string pluginPath;

    std::string pluginConfigPath;

    std::map<std::string, std::vector<FragmentSpecification>> inputFragmentSpecifications;

    std::map<std::string, std::vector<FragmentSpecification>> outputFragmentSpecifications;

    /**
     * Parses the given ptree into a vector of FragmentSpecs
     * @param ptree A boost property_tree object
     * @return A vector of FragmentSpecs
     */
    std::vector<FragmentSpecification> extractFragmentSpecs(boost::property_tree::ptree ptree);

    /**
     * Create a well readable string-representation of the Plugin by stripping off the file path and type extension
     * from ´pluginPath´. It does not guarantee uniqueness in a set of plugins and should therefor not be used to
     * identify or compare objects.
     * @return A displayable string-representation of the Plugin
     */
    std::string getDisplayName();
};

#endif //LIVEKIT_PLUGINSPEC_H
