#include "PluginSpecification.h"

#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

using namespace std;

PluginSpecification::PluginSpecification(const string &pluginPath, const string &pluginConfigPath)
        : pluginPath{pluginPath}
        , pluginConfigPath{pluginConfigPath}
        , inputFragmentSpecifications{
            {"preprocessing",   {}},
            {"cycle",           {}},
            {"postprocessing",  {}}}
        , outputFragmentSpecifications{
            {"preprocessing",   {}},
            {"cycle",           {}},
            {"postprocessing",  {}}} {}

PluginSpecification::PluginSpecification(const string &pluginPath,
                                         const string &pluginConfigPath,
                                         const map<string, vector<FragmentSpecification>> &inputFragmentSpecifications,
                                         const map<string, vector<FragmentSpecification>> &outputFragmentSpecifications)
        : pluginPath{pluginPath}
        , pluginConfigPath{pluginConfigPath}
        , inputFragmentSpecifications{inputFragmentSpecifications}
        , outputFragmentSpecifications{outputFragmentSpecifications} {}

PluginSpecification::PluginSpecification(const boost::property_tree::ptree &pluginPtree) {
    auto pluginPath = pluginPtree.get_child("pluginPath").get_value<string>();

    if (pluginPath.empty()) // 'pluginPath' is the only value required
        throw runtime_error("Could not find value for key 'pluginPath'");

    string pluginConfigPath;
    if (pluginPtree.get_child_optional("configPath")) {
        pluginConfigPath = pluginPtree.get_child("configPath").get_value<string>();
    } else pluginConfigPath = "";

    map<string, vector<PluginSpecification::FragmentSpecification>> inputSpecifications, outputSpecifications;
    for (auto name : {"preprocessing", "cycle", "postprocessing"}) {
        auto inputFragmentNode = pluginPtree.get_child("inputFragments").get_child_optional(name);
        if (inputFragmentNode)
            inputSpecifications[name] = this->extractFragmentSpecs(inputFragmentNode.get());
        else
            inputSpecifications[name] = {};

        auto outputFragmentNode = pluginPtree.get_child("outputFragments").get_child_optional(name);
        if (outputFragmentNode)
            outputSpecifications[name] = this->extractFragmentSpecs(outputFragmentNode.get());
        else
            outputSpecifications[name] = {};

        for (auto const &outputSpecification : outputSpecifications[name])
            if (outputSpecification.fromPrecedingCycle)
                throw runtime_error("An output fragment is not allowed to set the fromPrecedingCycle flag");
    }

    // Delegate to base constructor
    // TODO: is this really how we want to do this?
    new(this) PluginSpecification(pluginPath, pluginConfigPath, inputSpecifications, outputSpecifications);
}

string PluginSpecification::getDisplayName() {
    boost::filesystem::path p(this->pluginPath);
    return p.stem().string();
}

vector<PluginSpecification::FragmentSpecification>
PluginSpecification::extractFragmentSpecs(boost::property_tree::ptree ptree) {
    vector<FragmentSpecification> specifications;
    BOOST_FOREACH(const boost::property_tree::ptree::value_type &fragment, ptree) {
        for (auto &fragmentSpecification : ptree) {
            auto fragmentName = fragmentSpecification.second.get_child("name").get_value<string>();
            auto fragmentType = fragmentSpecification.second.get_child("type").get_value<string>();
            bool fromPrecedingCycle = fragment.second.get_child_optional("fromPrecedingCycle")
                                      && fragment.second.get_child("fromPrecedingCycle").get_value<bool>();

            specifications.push_back({fragmentName, fragmentType, fromPrecedingCycle});
        }
    }
    return specifications;
}
