#include "Configurable.h"

using namespace std;


void Configurable::setConfigFromFile(const string &configFilePath) {
    boost::property_tree::read_json(configFilePath, this->parsedConfigTree);

    this->setConfig();
}

void Configurable::setConfigFromFile(const string &configFilePath, map<string, boost::any> fallbackConfigMap) {
    this->configMap.insert(fallbackConfigMap.begin(), fallbackConfigMap.end());
    boost::property_tree::read_json(configFilePath, this->parsedConfigTree);
    this->setConfig();
}

bool Configurable::containsConfigEntry(const string& key) const {
    // throws boost::bad_get exception if you try to get an existing key using a wrong type
    return this->configMap.find(key) != this->configMap.end();
}
