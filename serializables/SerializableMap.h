#ifndef LIVEKIT_SERIALIZABLEMAP_H
#define LIVEKIT_SERIALIZABLEMAP_H

#include <map>

template<typename K, typename V>
class SerializableMap : public Serializable {
private:
    std::map<K,V> map;
public:
    T& operator [](int index) {
        return map[index];
    }
};

#endif //LIVEKIT_SERIALIZABLEMAP_H
