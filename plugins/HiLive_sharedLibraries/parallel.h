#ifndef PARALLEL_H
#define PARALLEL_H

#include "headers.h"
#include "definitions.h"
#include "tools.h"
#include "kindex.h"

//------ Threading tools --------------------------------------------//

/**
 * Task data structure. Contains all information for a thread to process a BCL file.
 * @author Martin Lindner
 */
struct Task {
  /** The lane of the task. */
  uint16_t lane;
  /** The tile of the task. */
  uint16_t tile;
  /** Struct containing the read properties (Barcode vs. sequence; length; mate). */
  SequenceElement seqEl;
  /** Current cycle of the particular read (in general, this does NOT equal the sequencing cycle!). Must be <=seqEl.length. */
  int cycle;

  /**
   * Constructor for a NULL task.
   * @author Tobias Loka
   */
  Task() : lane(255), tile(0), seqEl(NULLSEQ), cycle(0) {};

  /**
   * Constructor for a valid task.
   * @param ln The lane number.
   * @param tl The tile number.
   * @param seq The respective seqEl element for the current read containing information about length, type (barcode vs. sequence), mate number ...
   * @param cl The cycle of the current read (in general, this does NOT equal the sequencing cycle!). Must be <=seqEl.length.
   * @author Martin Lindner
   */
 Task(uint16_t ln, uint16_t tl, SequenceElement seq, int cl):
	 lane(ln), tile(tl), seqEl(seq), cycle(cl) {};

 /** Constructor for a task without seqEl information. */
 Task(uint16_t ln, uint16_t tl, int cl) : lane(ln), tile(tl), seqEl(NULLSEQ), cycle(cl) {};

  /**
   * Overload of the << operator. Defines the cout form of a task.
   * @author Martin Lindner
   */
  friend std::ostream& operator<<(std::ostream& os, const Task& t);

};

/**
 * Overload of the == operator.
 * @return true, if all fields/variables of the compared tasks equal.
 * @author Martin Lindner
 */
inline bool operator==(const Task& l, const Task& r){ return (r.lane==l.lane)&&(r.tile==l.tile)&&(r.cycle==l.cycle)&&(r.seqEl==l.seqEl); }

/**
 * Overload of the != operator.
 * @return true, if at least one field/variable of the compared tasks is different.
 * @author Martin Lindner
 */
inline bool operator!=(const Task& l, const Task& r){ return !(l==r); }

inline bool operator<(const Task& l, const Task& r){
	if ( l.cycle == r.cycle) {
		if ( l.lane == r.lane ) {
			if ( l.tile == r.tile ) {
				return l.seqEl.mate < r.seqEl.mate;
			} else {
				return l.tile < r.tile;
			}
		} else {
			return l.lane < r.lane;
		}
	} else {
		return l.cycle < r.cycle;
	}
}

/**
 * Definition of a NULL task.
 * @author Martin Lindner
 */
const Task NO_TASK (255,0,NULLSEQ,0);

// Task queue data structure. Manages a list of task objects in a thread safe way.
class TaskQueue {
  // the internal queue
  std::queue<Task> tasks;
  
  // mutex to ensure that only one process can access the queue at once
  std::mutex m;

 public:
  // Add element to the task list
  void push(Task t);
  
  // Get element from the task list
  Task pop();

  // return the size of the queue
  uint64_t size();
};

// Agenda item status
typedef uint8_t ItemStatus;
const ItemStatus WAITING = 0;
const ItemStatus BCL_AVAILABLE = 1;
const ItemStatus RUNNING = 2;
const ItemStatus FINISHED = 3;
const ItemStatus RETRY = 4;
const ItemStatus FAILED = 5;
const ItemStatus ERROR = std::numeric_limits<ItemStatus>::max();


#endif /* PARALLEL_H */