#ifndef LIVEKIT_SEQUENCEELEMENT_H
#define LIVEKIT_SEQUENCEELEMENT_H


///////////////////////////////////////
////////// Sequence Elements //////////
///////////////////////////////////////

/**
 * Information about the sequences.
 * One element can be a read or a barcode.
 * @author Tobias Loka
 */
struct SequenceElement {

    /** The id of the read. Equals the position in the argument list and in the AlignmentSettings::seqs vector (0-based). */
    uint16_t id;

    /** The mate number. 0 for barcodes, increasing for sequence reads in the given order (1-based). */
    uint16_t mate;

    uint16_t readId;

    /** The length of the respective read. */
    uint16_t length;

    /**
     * Constructor of a SequenceElement NULL object.
     * @author Tobias Loka
     */
    SequenceElement () : id(0), mate(0), readId(0), length(0) {};

    /**
     * Constructor of a valid SequenceElement object.
     * @param id The id of the read.
     * @param m The mate number of the read (0 for barcodes, incrementing for sequence reads)
     * @param l The length of the read
     * @author Tobias Loka
     */
    SequenceElement (uint16_t id, uint16_t mate, uint16_t readId, uint16_t length): id(id), mate(mate), readId(readId), length(length) {};

    /**
     * Check whether the SequenceElement object is a barcode or not.
     * @return true, if SequenceElement is a barcode. False if not.
     * @author Tobias Loka
     */
    bool isBarcode() const { return (mate==0);}
};

/**
 * Check if two Sequence elements are equal.
 */
inline bool operator==(const SequenceElement l, const SequenceElement r) {return (l.length==r.length) && (l.mate==r.mate) && (l.id==r.id);}

/**
 * Checks if two sequence elements are not equal.
 */
inline bool operator!=(const SequenceElement l, const SequenceElement r) {return !(l==r);}

/**
 * An undefined sequence element (NULL element).
 */
const SequenceElement NULLSEQ = SequenceElement();


#endif //LIVEKIT_SEQUENCEELEMENT_H
