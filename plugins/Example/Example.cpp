#include "Example.h"

using namespace std;

void Example::init() {
    this->permanentFragment = this->framework->createNewFragment("exampleFragment");
    this->permanentFragment->set("lastLetter", 0);
}

FragmentContainer *Example::runCycle(FragmentContainer *inputFragments) {
    (void) inputFragments; //UNUSED

    int currentValue = this->permanentFragment->get<int>("lastLetter");
    //cout << char('A' + (currentValue % 26)) << endl;
    this->permanentFragment->set<int>("lastLetter", currentValue + 1);

    auto fragmentContainer = this->framework->createNewFragmentContainer();
    auto fragment = fragmentContainer->add(this->framework->createNewFragment("communication"));

    fragment->set("val", currentValue + 1);

    return fragmentContainer;  // should return outputFragments
}

void Example::setConfig() {
    this->registerConfigEntry<int>("lane", 1);
    this->registerConfigEntry<int>("tile", 1101);
    this->registerConfigEntry<string>("read", "100R,4B,4B,100R");
}

extern "C" Example *create() {
    return new Example;
}

extern "C" void destroy(Example *plugin) {
    delete plugin;
}
