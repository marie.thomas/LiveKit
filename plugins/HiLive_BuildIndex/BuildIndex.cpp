#include "BuildIndex.h"

using namespace std;

void BuildIndex::init() {

}

FragmentContainer *BuildIndex::runPreprocessing(FragmentContainer *inputFragments) {
    (void) inputFragments; // UNUSED
    
    cout
        << "-----------------------------------------------------------------------------------------------" << endl
        << "HiLive Index Builder v" << HiLive_VERSION_MAJOR << "." << HiLive_VERSION_MINOR
        << " PLUGIN VERSION - Build Index for Realtime Alignment of Illumina Reads" << endl
        << "-----------------------------------------------------------------------------------------------" << endl;


    SerializableIndex *index;

    if (this->getConfigEntry<bool>("storeIndex")) {
        string filename = this->getConfigEntry<string>("indexName") + ".fragment";
        if (boost::filesystem::exists( filename )) {
            FILE *serializedFile = fopen(filename.c_str(), "rb");

            unsigned long size;
            fread(&size, sizeof(unsigned long), 1, serializedFile);

            char *serializedSpace = (char *) malloc(size);
            fread(serializedSpace, size, 1, serializedFile);

            cout << "Load FM-index from file " << this->getConfigEntry<string>("indexName") << endl;

            index = new SerializableIndex(size, serializedSpace, false);

            fclose(serializedFile);

        } else {
            // TODO: Currently the index is being deleted, as soon as serialize is called by the Framework.
            // TODO: As soon as we implement continue-Functionality, this should be fixed

            cout << "Build index from Reference " << this->getConfigEntry<string>("fastaName") << endl;

            index = new SerializableIndex(
                this->getConfigEntry<string>("fastaName"),
                this->getConfigEntry<string>("indexName"),
                !this->getConfigEntry<bool>("doNotConvertSpaces"),
                this->getConfigEntry<bool>("trimAfterSpace")
            );

            unsigned long size = index->serializableSize();
            char *serializedSpace = (char *) malloc(sizeof(char) * size);
            index->backup(serializedSpace);

            FILE *serializeFile = fopen(filename.c_str(), "wb");
            fwrite(&size, sizeof(unsigned long), 1, serializeFile);
            fwrite(serializedSpace, size, 1, serializeFile);

            free(serializedSpace);
            fclose(serializeFile);
        }
    } else {

        cout << "Build index from Reference " << this->getConfigEntry<string>("fastaName") << endl;

        index = new SerializableIndex(
            this->getConfigEntry<string>("fastaName"),
            this->getConfigEntry<string>("indexName"),
            !this->getConfigEntry<bool>("doNotConvertSpaces"),
            this->getConfigEntry<bool>("trimAfterSpace")
        );
    }

    auto fragmentContainer = this->framework->createNewFragmentContainer();
    fragmentContainer->add(this->framework->createNewFragment("FMIndex"));
    shared_ptr<Fragment> fmIndexFragment = fragmentContainer->get("FMIndex");
    fmIndexFragment->setSerializable("FMIndex", index);

    return fragmentContainer;
}

void BuildIndex::finalize() {

}

void BuildIndex::setConfig() {
    this->registerConfigEntry<string>("fastaName", "../tutorial/hiv1.fa");
    this->registerConfigEntry<string>("indexName", "../temp/index");
    this->registerConfigEntry<bool>("storeIndex", true);
    this->registerConfigEntry<bool>("doNotConvertSpaces", false);
    this->registerConfigEntry<bool>("trimAfterSpace", false);
}

extern "C" BuildIndex *create() {
    return new BuildIndex;
}

extern "C" void destroy(BuildIndex *plugin) {
    delete plugin;
}

