#ifndef LIVEKIT_BUILDINDEX_H
#define LIVEKIT_BUILDINDEX_H

#include <iostream>

#include <boost/filesystem/operations.hpp>

#include "../../framework/Framework.h"
#include "../../framework/fragments/Fragment.cpp"
#include "../../serializables/SerializableIndex.h"

class BuildIndex final : public Plugin {
public:
    void init() override;

    FragmentContainer *runPreprocessing(FragmentContainer *inputFragments) override;

    FragmentContainer *runCycle(FragmentContainer *inputFragments) override { return inputFragments; };

    FragmentContainer *runFullReadPostprocessing(FragmentContainer *inputFragments) override { return inputFragments; };

    void setConfig() override;

    void finalize() override;
};

#endif //LIVEKIT_BUILDINDEX_H
