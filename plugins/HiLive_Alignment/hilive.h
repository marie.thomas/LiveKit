#ifndef HILIVE_HILIVE_H

#define HILIVE_HILIVE_H

#include "../HiLive_sharedLibraries/headers.h"
#include "../HiLive_sharedLibraries/definitions.h"
#include "../HiLive_sharedLibraries/global_variables.h"
#include "../HiLive_sharedLibraries/kindex.h"
#include "../HiLive_sharedLibraries/alnstream.h"
#include "../HiLive_sharedLibraries/alnout.h"
#include "../HiLive_sharedLibraries/parallel.h"
#include "../HiLive_sharedLibraries/tools_static.h"
#include "../../serializables/SerializableSequenceElements.h"
#include "../../framework/Plugin.h"
#include "../../framework/FrameworkInterface.h"
#include "../../framework/fragments/Fragment.cpp"
#include "../../serializables/SerializableIndex.h"
#include "../../serializables/SerializableAlignment.h"

class Hilive final : public Plugin {
private:
    time_t t_start;

    SerializableSequenceElements *sequenceElements;

    map<uint16_t, map<uint16_t, map<uint16_t, SerializableAlignment *>>> alignments;

    bool isOutputCycle(int cycle);

public:
    void init() override;

    FragmentContainer *runPreprocessing(FragmentContainer *inputFragments) override;

    FragmentContainer *runCycle(FragmentContainer *inputFragments) override;

    FragmentContainer *runFullReadPostprocessing(FragmentContainer *inputFragments) override { return inputFragments; };

    void setConfig() override;

    void finalize() override;

    // lambda functions for setConfig
    /**
     * @return a function that sets the alignment mode for sequencing [(ALL)(UNIQUE)(BESTN[0-9]+)(ALLBEST)(ANYBEST)AU(N[0-9]+)HB]
     * the parameter of the returned function is an Alignment mode as string.
     */
    std::function<OutputMode (std::string)> setMode();

    void setSequenceElements();

    std::function<uint64_t (std::string)> setBlockSize();

    std::function<uint16_t ()> setAnchorLength(SerializableIndex *index);
};

#endif
