#include "../catch2/catch.hpp"
#include "../fakeit/fakeit.hpp"
#include "../../plugins/ReadPositionParser/ReadPositionParser.h"
#include "../../framework/fragments/Fragment.h"
#include "../../framework/Plugin.h"
#include "../../framework/Framework.h"
#include "../../framework/basecalls/data_representation/BclRepresentation.h"
#include "../../serializables/SerializableVector.h"

using namespace std;

TEST_CASE("ReadPositionParser") {

    ReadPositionParser positionParser;

    shared_ptr<Fragment> fragment;

    SECTION("Should parse the *_pos.txt file correctly") {

        string manifestFilePath = "../tests/mockFiles/ReadPositionParserTestFiles/ReadPositionFramework_pos.json";
        auto *framework = new Framework(manifestFilePath);
        positionParser.setFramework(framework->getFrameworkInterface());
        positionParser.setConfigFromFile("../tests/mockFiles/ReadPositionParserTestFiles/mockReadPositionParser_pos.json");

        //This is necessary because the union of the framework configMap and the plugin configMap doesn't work here
        positionParser.registerConfigEntry<vector<uint16_t>, string>("lanes", "1", Configurable::toVector<uint16_t>(','));
        positionParser.registerConfigEntry<vector<uint16_t>, string>("tiles", "1101", Configurable::toVector<uint16_t>(','));
        positionParser.registerConfigEntry<int>("laneCount", (int) positionParser.getConfigEntry<vector<uint16_t>>("lanes").size());
        positionParser.registerConfigEntry<int>("tileCount", (int) positionParser.getConfigEntry<vector<uint16_t>>("tiles").size());
        positionParser.registerConfigEntry<string>("BclParserType", "BclParser");

        FragmentContainer *container = positionParser.runPreprocessing(nullptr);
        fragment = container->get("POSITION");

        auto serializableVector = dynamic_cast<SerializableVector<float> *>(fragment->getSerializable(
                "positionVector"));

        REQUIRE (serializableVector->at(0) == 7.5f);
        REQUIRE (serializableVector->at(4) == 18.4f);
        REQUIRE (serializableVector->at(20) == 17.5f);
        REQUIRE (serializableVector->at(38) == 1.8f);

        REQUIRE (serializableVector->at(1) == 0.1f);
        REQUIRE (serializableVector->at(3) == 0.8f);
        REQUIRE (serializableVector->at(13) == 2.6f);
        REQUIRE (serializableVector->at(15) == -2.8f);

        delete framework;
    }
    SECTION("Should parse the .locs file correctly") {

        string manifestFilePath = "../tests/mockFiles/ReadPositionParserTestFiles/ReadPositionFramework_locs.json";
        auto *framework = new Framework(manifestFilePath);
        positionParser.setFramework(framework->getFrameworkInterface());
        positionParser.setConfigFromFile("../tests/mockFiles/ReadPositionParserTestFiles/mockReadPositionParser_locs.json");

        //This is necessary because the union of the framework configMap and the plugin configMap doesn't work here
        positionParser.registerConfigEntry<vector<uint16_t>, string>("lanes", "1", Configurable::toVector<uint16_t>(','));
        positionParser.registerConfigEntry<vector<uint16_t>, string>("tiles", "1101", Configurable::toVector<uint16_t>(','));
        positionParser.registerConfigEntry<int>("laneCount", (int) positionParser.getConfigEntry<vector<uint16_t>>("lanes").size());
        positionParser.registerConfigEntry<int>("tileCount", (int) positionParser.getConfigEntry<vector<uint16_t>>("tiles").size());
        positionParser.registerConfigEntry<string>("BclParserType", "BclParser");

        FragmentContainer *container = positionParser.runPreprocessing(nullptr);

        fragment = container->get("POSITION");

        auto serializableVector = dynamic_cast<SerializableVector<float> *>(fragment->getSerializable(
                "positionVector"));

        REQUIRE (serializableVector->at(0) == 18030.0f);
        REQUIRE (serializableVector->at(1) == 1640.0f);
        REQUIRE (serializableVector->at(2) == 17603.0f);

        delete framework;
    }
    SECTION("Should parse the .clocs file correctly") {

        string manifestFilePath = "../tests/mockFiles/ReadPositionParserTestFiles/ReadPositionFramework_clocs.json";
        auto *framework = new Framework(manifestFilePath);
        positionParser.setFramework(framework->getFrameworkInterface());
        positionParser.setConfigFromFile("../tests/mockFiles/ReadPositionParserTestFiles/mockReadPositionParser_clocs.json");

        //This is necessary because the union of the framework configMap and the plugin configMap doesn't work here
        positionParser.registerConfigEntry<vector<uint16_t>, string>("lanes", "1", Configurable::toVector<uint16_t>(','));
        positionParser.registerConfigEntry<vector<uint16_t>, string>("tiles", "1101", Configurable::toVector<uint16_t>(','));
        positionParser.registerConfigEntry<int>("laneCount", (int) positionParser.getConfigEntry<vector<uint16_t>>("lanes").size());
        positionParser.registerConfigEntry<int>("tileCount", (int) positionParser.getConfigEntry<vector<uint16_t>>("tiles").size());
        positionParser.registerConfigEntry<string>("BclParserType", "BclParser");

        FragmentContainer *container = positionParser.runPreprocessing(nullptr);

        fragment = container->get("POSITION");

        auto serializableVector = dynamic_cast<SerializableVector<float> *>(fragment->getSerializable(
            "positionVector"));

        REQUIRE (serializableVector->at(2) == 41.9f);
        REQUIRE (serializableVector->at(4) == 74.9f);
        REQUIRE (serializableVector->at(10) == 148.2f);

        delete framework;
    }
}
