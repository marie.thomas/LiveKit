#include "../framework/FileDetector.h"
#include "./catch2/catch.hpp"
#include <string>
#include <iostream>
#include <pthread.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <utility>

#define ROOT "../tests"

bool waitForDirectoryDetection(std::string path) {
    FileDetector fileDetector(ROOT);
    fileDetector.checkForDirectory(std::move(path));
    return true;
}

bool waitForLaneDirectoryDetection(int lanes) {
    FileDetector fileDetector(ROOT);
    fileDetector.checkForAllLanes(lanes);
    return true;
}

bool waitForCycleDirectoryDetection(int lanes, int cycle) {
    FileDetector fileDetector(ROOT);
    fileDetector.checkForCycleFiles(lanes, cycle, false);
    return true;
}

void *makeDirectories(void *td) {
    auto *directoryNames = static_cast<std::vector<std::string>*>(td);
    for (const auto& name : *directoryNames) {
        std::string path = ROOT + name;
        int status = mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        CHECK(status >= 0);
    }

    return nullptr;
}

void removeDirectories(std::vector<std::string> directoryNames) {
    for (const auto& name : directoryNames) {
        boost::filesystem::remove_all(ROOT + name);
    }
}

TEST_CASE("Test the FileDetector class") {
    SECTION("Test directory detection") {
        std::vector<std::string> directoryNames = {"/a","/b","/c"};

        pthread_t t;

        int threadStatus;

        if ((threadStatus = pthread_create(&t, NULL, makeDirectories, &directoryNames))) {
            CHECK(threadStatus == 0);
        }

        for (const auto& name : directoryNames) {
            std::string path = ROOT + name;
            REQUIRE(waitForDirectoryDetection(path));
        }

        pthread_join(t, NULL);

        removeDirectories(directoryNames);
    }

    SECTION("Test lane and cycle directory detection") {
        //Check for lane folders
        int lanes = 5;
        std::vector<std::string> laneDirectoryNames;

        for (int lane = 1; lane <= lanes; lane++) {
            laneDirectoryNames.push_back("/L00" + std::to_string(lane));
        }

        pthread_t t[2];

        int threadStatus;

        if ((threadStatus = pthread_create(&t[0], NULL, makeDirectories, &laneDirectoryNames))) {
            CHECK(threadStatus == 0);
        }

        REQUIRE(waitForLaneDirectoryDetection(lanes));

        //Check for cycle folders
        int cycles = 5;
        std::vector<std::string> cycleDirectoryNames;

        for (const auto& lane : laneDirectoryNames) {
            for (int cycle = 1; cycle <= cycles; cycle++) {
                cycleDirectoryNames.push_back(lane + "/C" + std::to_string(cycle) + ".1");
            }
        }

        if ((threadStatus = pthread_create(&t[1], NULL, makeDirectories, &cycleDirectoryNames))) {
            CHECK(threadStatus == 0);
        }

        for (int cycle = 1; cycle <= cycles; cycle++) {
            REQUIRE(waitForCycleDirectoryDetection(lanes, cycle));
        }

        pthread_join(t[0], NULL);
        pthread_join(t[1], NULL);

        removeDirectories(laneDirectoryNames);
    }
}