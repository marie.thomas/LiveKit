#include <iostream>
#include <vector>
#include "../framework/basecalls/data_representation/BclRepresentation.h"
#include "../framework/basecalls/parser/BclParser.h"
#include "./catch2/catch.hpp"

TEST_CASE("BclRepresentationTests") {
    std::vector<uint16_t> lanes = {1};
    std::vector<uint16_t> tiles = {1101};
    BclRepresentation bclRepresentation(lanes, tiles); // 1 lane, 1 tiles
    uint16_t lane = 1; // first lane
    uint16_t tile = 1101; // first tile

    SECTION("Should be able to set the buffer size") {
        bclRepresentation.setBufferSize(5);
        REQUIRE(bclRepresentation.getBufferSize() == 5);
        bclRepresentation.setBufferSize(10);
        REQUIRE(bclRepresentation.getBufferSize() == 10);
    }

    SECTION("Should be able to add BCLs") {
        for (int i = 0; i < 3; i++) { // this simulates multiple cycles
            bclRepresentation.addBcl(1, 1101, BCL(1, (char) 65 + i)); // adds an BCL with only one char (some ascii characters: A, B, C)
        }

        REQUIRE(bclRepresentation.getBcl(lane, tile, 0) == BCL(1, 'C')); // last added
        REQUIRE(bclRepresentation.getBcl(lane, tile, 1) == BCL(1, 'B')); // second last added
        REQUIRE(bclRepresentation.getBcl(lane, tile, 2) == BCL(1, 'A')); // first added

        REQUIRE(bclRepresentation.getMostRecentBcls(1).size() == 1); // because we have only one tile per lane
        REQUIRE(bclRepresentation.getMostRecentBcls(1)[0] == BCL(1, 'C')); // last added

        REQUIRE(bclRepresentation.getBclData().size() == 1); // one lane
        REQUIRE(bclRepresentation.getBclData()[1].size() == 1); // one tile
        REQUIRE(bclRepresentation.getBclData()[1][1101].size() == 3); // three cycles
        REQUIRE(bclRepresentation.getBclData()[1][1101][0] == BCL(1, 'C')); // last added
        REQUIRE(bclRepresentation.getBclData()[1][1101][1] == BCL(1, 'B')); // second last added
        REQUIRE(bclRepresentation.getBclData()[1][1101][2] == BCL(1, 'A')); // first added
    }

    SECTION("Should remove BCLs if the buffer is to full") {
        bclRepresentation.setBufferSize(2);

        for (int i = 0; i < 5; i++) { // this simulates multiple cycles
            bclRepresentation.addBcl(1, 1101, BCL(1, (char) 65 + i)); // adds an BCL with only one char (some ascii characters: A, B, C, D, E)
        }

        REQUIRE(bclRepresentation.getBcls(lane, tile).size() == 2);
        REQUIRE(bclRepresentation.getBcl(lane, tile, 0) == BCL(1, 'E')); // last added
        REQUIRE(bclRepresentation.getBcl(lane, tile, 1) == BCL(1, 'D')); // second last added
    }

}