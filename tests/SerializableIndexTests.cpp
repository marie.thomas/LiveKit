#include "./catch2/catch.hpp"
#include "./fakeit/fakeit.hpp"

#include "../serializables/SerializableIndex.h"

TEST_CASE("SerializableIndex") {
    std::string mockFilename = "../tests/mockFiles/mock_fasta.fa";
    std::string indexFolder = "../tests/mockFiles/index/";
    std::string indexFile = indexFolder + "hiv";

    // setup
    namespace fs = boost::filesystem;
    fs::create_directory(indexFolder);

    /*
     * The header of the sequences that are indexed look like this:
     * >NC_001802.1 Human immunodeficiency virus 1, part1
     * >NC_001803.1 Human immunodeficiency virus 1, part2
     *
     * The first symbol (">") marks this as a header, the rest is potentially the name.
     * We can set parameter if we want to trim the name to the first word or convert spaces into underscores.
     *
     */

    SECTION("Should be able to create an index for the DNA (do not trim ID)") {
        SerializableIndex index(mockFilename, indexFolder, false, false);

        REQUIRE(index.getSeqNames()[0] == "NC_001802.1 Human immunodeficiency virus 1, part1");
        REQUIRE(index.getSeqNames()[1] == "NC_001803.1 Human immunodeficiency virus 1, part2");
    };

    SECTION("Should be able to create an index for the DNA (trim ID)") {
        SerializableIndex index(mockFilename, indexFolder, false, true);

        REQUIRE(index.getSeqNames()[0] == "NC_001802.1");
        REQUIRE(index.getSeqNames()[1] == "NC_001803.1");
    };

    SECTION("Should be able to create an index for the DNA (convert spaces)") {
        SerializableIndex index(mockFilename, indexFolder, true, false);

        REQUIRE(index.getSeqNames()[0] == "NC_001802.1_Human_immunodeficiency_virus_1,_part1");
        REQUIRE(index.getSeqNames()[1] == "NC_001803.1_Human_immunodeficiency_virus_1,_part2");
    };

    // TODO: Implement appropriate tests to check, if the Index has been serialized/deserialized successfully

    // clean up
    if (fs::exists(indexFolder))
        if (!fs::remove_all(indexFolder))
            std::cerr << "Error deleting directory: " + indexFolder << std::endl;

}
